<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="material" Label="Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="KinematicViscosity" Label="Kinematic viscosity (nu)" Optional="false">
          <!-- specified in constant/transportProperties -->
          <BriefDescription>Kinematic viscosity</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
          </Categories>
          <DefaultValue>1.5e-05</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
