<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Omega3P</Cat>
    <Cat>S3P</Cat>
    <Cat>T3P</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Epsilon" Label="Relative Permittivity (Epsilon)" Version="0" >
          <BriefDescription>Real Component of Relative Permittivity</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <Double Name="ImgEpsilon" Label="Imaginary Relative Permittivity" Version="0" Optional="true" >
          <BriefDescription>Imaginary Component of Relative Permittivity associated with material loss</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Max Inclusive="true">0</Max>
          </RangeInfo>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <Double Name="Mu" Label="Relative Permeability (Mu)" Version="0" >
          <BriefDescription>Real Component of Relative Permeability</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <Double Name="ImgMu" Label="Imaginary Relative Permeability" Version="0" Optional="true" >
          <BriefDescription>Imaginary Component of Relative Permeability associated with material loss</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Max Inclusive="true">0</Max>
          </RangeInfo>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <Double Name="Sigma" Label="Conductivity" Version="0" Units="S/m">
          <BriefDescription>Conductivity of lossy material</BriefDescription>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
